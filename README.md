## INVITE TOOLCHECK KIPerWeb-Prototypen

Copyright (C) 2022 - Benjamin Paaßen  
Educational Technology Lab  
Deutsches Forschungszentrum für Künstliche Intelligenz GmbH  

This prototype is only intended for use within the INVITE TOOLCHECK.  
Further use or copy of source code is forbidden.  

In case of questions, please contact us at  
[benjamin.paassen@dfki.de](mailto:benjamin.paassen@dfki.de)
